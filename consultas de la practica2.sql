﻿USE trabajador;

-- 1 indicar el numero de ciudades que hay en la tabla ciudades
SELECT COUNT(*) numCiudades FROM ciudad c;

-- 2 indicar el nombre de las ciudades que tengan una poblacion por encima de la población media
 SELECT AVG( c.población) FROM ciudad c ; 
SELECT nombre FROM ciudad c WHERE c.población>( SELECT AVG( c.población) FROM ciudad c) ;

-- 3 indicar el nombre de las ciudades que tengan una población por debajo de la media
  SELECT AVG( c.población) FROM ciudad c;
  SELECT c.nombre FROM ciudad c WHERE c.población<(SELECT AVG( c.población) FROM ciudad c);
  
-- 4 indicar el nombre de la ciudad con la población máxima
   SELECT MAX( c.población) FROM ciudad c;
  SELECT c.nombre FROM ciudad c WHERE c.población=(SELECT MAX( c.población) FROM ciudad c);
  
-- 5 indicar el nombre de la ciudad con la población mínima
  SELECT MIN(c.población) FROM ciudad c;
  SELECT c.nombre FROM ciudad c WHERE c.población=(SELECT MIN(c.población) FROM ciudad c);
  
-- 6 indicar el número de ciudades que tengan una población por encima de la población media
  SELECT AVG( c.población) FROM ciudad c;
  SELECT COUNT(*) ciudadesMayorQueEdadMedia FROM ciudad c WHERE c.población>(SELECT AVG( c.población) FROM ciudad c);
  
-- 7 indícame el número de personas que viven en cada ciudad
  SELECT COUNT(*) numPersonas FROM persona p GROUP BY p.ciudad; 
  
-- 8 utilizando la tala trabaja indicarme cuantas personas trabajan en cada una de las compañías
  SELECT t.compañia, COUNT(*) personasTrabajan FROM trabaja t GROUP BY t.compañia;
  
-- 9 indicarme la compañía que más trabajadores tiene
  -- subconsulta c1
   SELECT COUNT(*) trbajadores, t.compañia FROM trabaja t GROUP BY t.compañia;
  -- subconsulta c2
    SELECT MAX( c1.trbajadores) maximo_de_trabajadores FROM (SELECT COUNT(*) trbajadores, t.compañia FROM trabaja t GROUP BY t.compañia) c1;
  -- consulta final
    SELECT c1.compañia FROM (SELECT COUNT(*) trbajadores, t.compañia FROM trabaja t GROUP BY t.compañia) c1
      JOIN (SELECT MAX( c1.trbajadores) maximo_de_trabajadores FROM (SELECT COUNT(*) trbajadores, t.compañia FROM trabaja t GROUP BY t.compañia) c1) c2
      ON c1.trbajadores= c2.maximo_de_trabajadores; 
  
-- 10 indicar el salario medio de cada una de las compañias
  SELECT AVG(t.salario) salarioMedio, t.compañia FROM trabaja t GROUP BY t.compañia;
  
-- 11 listarme el nombre de las personas y la población de la ciudad donde viven
  SELECT p.nombre, p.ciudad FROM ciudad c JOIN persona p ON p.ciudad= c.nombre; 

-- 12 listar el nombre de las personas, la calle donde vive y la población de la ciudad donde vive
  SELECT p.nombre, p.calle, c.población FROM persona p JOIN ciudad c ON p.ciudad= c.nombre;    
  
-- 13 listar el nombre de las personas, la ciudad donde vive y la ciudad donde está la compañía para la que trabaja
  SELECT p.nombre , p.ciudad ciudad_donde_vive, c.ciudad ciudad_donde_trabaja FROM persona p 
  JOIN trabaja t ON p.nombre= t.persona JOIN compañia c ON t.compañia= c.nombre;  
  
-- 14 realizar el algebra relaconal y explicar la siguiente consulta  
  SELECT * FROM persona p, trabaja t, compañia c WHERE p.nombre= t.persona
    AND t.compañia= c.nombre AND c.ciudad= p.ciudad; 
  
-- 15 listame el nombre de la persona y el nombre de su supervisor
  SELECT p.nombre, s.supervisor FROM supervisa s ; 
  
-- 16 listame el nombre de la persona, el nombre de su supervisor y las ciudades donde residen cada uno de ellos
  SELECT s.persona, p.ciudad AS ciudad_persona, s.supervisor, p1.ciudad AS ciudad_supervisa FROM persona p 
    JOIN supervisa s ON p.nombre= s.persona JOIN persona p1 ON p1.nombre= s.supervisor; 
  
-- 17 indicame el numero de ciudades distintas que hay en la tabla compañía
  SELECT DISTINCT COUNT(*) ciudades FROM compañia c;
  
-- 18 indicame el número de ciudades distintas que hay en la tabla personas
  SELECT DISTINCT COUNT(*) num_ciudades FROM persona p ;
  
-- 19 indicame el nombre de las personas que trabajan para fagor
  SELECT DISTINCT t.persona FROM trabaja t WHERE t.compañia='FAGOR'; 
 
-- 20  indicame el nombre de las personas que no trabajan para fagor 
 SELECT DISTINCT t.persona, t.compañia FROM trabaja t WHERE t.compañia<>'FAGOR'; 

-- 21 indicame el número de personas que trabajan para indra
  SELECT COUNT(*) numPersonas FROM trabaja t WHERE t.compañia='INDRA' ;
  
-- 22  indicame el número de personas que trabajan para indra y fagor
  SELECT COUNT(*) personasQueTrabajan, t.compañia FROM trabaja t GROUP BY t.compañia HAVING t.compañia='FAGOR' OR t.compañia='INDRA'; 
  
-- 23 listar la población donde vive cada persona, sus salario, su nombre y la compañía para la que trabaja.
--  Ordenar la salida por nombre de la persona y por salario de forma descendente      
   SELECT p.nombre, t.salario, c.población, t.compañia FROM persona p JOIN ciudad c ON c.nombre= p.ciudad 
    JOIN trabaja t ON p.nombre= t.persona ORDER BY t.salario DESC, p.nombre DESC;                                                     